import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
    {
        //startpage
        path:'/',
        name: 'Start',
        //lazyload 
        component: () => import (/* webpackChunkName: "start"*/ '../components/Start/Start.vue') 
    },
    {
        path:'/questions',
        name: 'Questions',
        props: true,
        //lazyload 
        component: () => import (/* webpackChunkName: "questions"*/ '../components/Quiz/Questions.vue') 


    },
    {
        path:'/results',
        name: 'Results',
        props: true,
        //lazyload 
        component: () => import (/* webpackChunkName: "results"*/ '../components/Result/Results.vue') 
    }

]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router;