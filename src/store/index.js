import Vue from 'vue'
import Vuex from 'vuex'
import { QuestionsAPI } from '@/components/Quiz/QuestionsAPI'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        questions: [],
        categories: [],
        error: ''

    },
    mutations: {
        setQuestions: (state, payload) => {
            state.questions = payload
        },
        setError: (state, payload) => {
            state.error = payload
        },
    },
    getters: {
        getQuestions: state => {
            return state.questions;
        }
    },
    actions: {
        async fetchQuestions({ commit }) {
            try {
                const questions = await QuestionsAPI.fetchQuestions()
                commit('setQuestions', questions)
                
            } catch (e) {
                commit('setError', e.message)
            }
        }


    }

})