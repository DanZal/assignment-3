export const QuestionsAPI = {
    fetchQuestion() {
        return fetch("https://opentdb.com/api.php?amount=10&category=10")
        .then(response => response.json())
        .then(data => data.data)
    }
}